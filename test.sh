#!/usr/bin/env bash
source ./config.txt

# Rename SSL certificates
# https://community.letsencrypt.org/t/how-to-get-crt-and-key-files-from-i-just-have-pem-files/7348
cd /etc/letsencrypt/live/reg.icomunity.com && \
cp privkey.pem domain.key && \
cat cert.pem chain.pem > domain.crt && \
chmod 777 domain.*

#create a testuser 
mkdir -p /mnt/docker-registry
docker run --entrypoint htpasswd registry:latest -Bbn icomunity $REGISTRY_PASSWORD > /mnt/docker-registry/passfile
  
# https://docs.docker.com/registry/deploying/
docker run -d -p 5060:5060 --restart=always --name icomunity_registry \
  -v /etc/letsencrypt/live/domain.reg.icomunity.com:/certs \
  -v /mnt/docker-registry:/var/lib/registry \
  -e REGISTRY_HTTP_TLS_CERTIFICATE=/certs/domain.crt \
  -e REGISTRY_HTTP_TLS_KEY=/certs/domain.key \
  -e REGISTRY_AUTH=htpasswd \
  -e "REGISTRY_AUTH_HTPASSWD_REALM=Icomunity Registry" \
  -e REGISTRY_AUTH_HTPASSWD_PATH=/var/lib/registry/passfile \
  registry:latest
  
# List images
curl https://icomunity:$REGISTRY_PASSWORD@domain.reg.icomunity.com/v2/_catalog