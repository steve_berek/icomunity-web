#!/bin/sh
source ./config.txt
echo "REGISTRY USERNAME $REGISTRY_USERNAME"

echo "LOGIN TO REGISTRY"
docker login reg.icomunity.com -u $REGISTRY_USERNAME -p $REGISTRY_PASSWORD

echo "BUILD DOCKER IMAGE"
docker build -t reg.icomunity.com/icomunity-web:latest .

echo "PUSH IMAGE TO REGISTRY"
docker push reg.icomunity.com/icomunity-web:latest