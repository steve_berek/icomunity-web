SSH_USERNAME="root"
SSH_IP="45.55.251.94"
#SSH_IP="192.168.105.21"

GREEN="\033[1;32m"
NO_COLOR="\033[0m"


echo "${GREEN} UPLOAD ICOMUNITY-WEB FRONT ZIP${NO_COLOR}"
rsync -v -r -e 'ssh' dist.zip $SSH_USERNAME@$SSH_IP:/var/www/icomunity-web/dist.zip
echo "${GREEN} ZIP UPLOADED ${NO_COLOR}"
