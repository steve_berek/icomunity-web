

export class NoticiaModel {
    id: string;
    title: string;
    description: string;
    created_at: Date;
    formattedDate: string;
}
