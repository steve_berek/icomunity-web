import { UserModel } from './vecino.model';

export class IncidenciaModel {
  id: string;
  code_comunity: string;
  image: string;
  category: string;
  type: string;
  username: string;
  user_email?: string;
  adress: string;
  description: string;
  created_at: Date;
  formattedDate: string;
  create: string;
  state: any;
  oculto: any;
  user?: UserModel;
}
