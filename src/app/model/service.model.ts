

export class ServiceModel {
    id: number;
    active: number;
    fk_code_comunity: string;
    type: string;
    email: string;
    name: string;
    phone: string;
    created_at: string;
}
