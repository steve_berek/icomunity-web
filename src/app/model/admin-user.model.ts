import { CompanyModel } from './company.model';
import { ComunidadModel } from './comunidad.model';

export class AdminUserModel {
  id?: string;
  email: string;
  name: string;
  phone: string;
  mainRole: string;
  roles: string[];
  comunities: ComunidadModel[];
  company?: CompanyModel;
  lastConnection?: Date;
  permissions?: any;
  companyUsers?: any[];

  constructor(initialValues?: Partial<AdminUserModel> | null) {
    if (initialValues) {
      Object.assign(this, initialValues);
    }
  }
}
