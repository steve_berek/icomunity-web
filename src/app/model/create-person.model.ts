

export class CreatePersonModel {
    id?: string;
    title: string;
    description: string;
    type: string;
    picture: File;
}
