import { PAGES_TYPE_ENUM } from '../common/enums/pages-type.enum';

export class SideNavMenuItem {
  icon: string;
  label: string;
  name: PAGES_TYPE_ENUM;
  route: string;
  active: boolean;
  hidden: boolean;
}
