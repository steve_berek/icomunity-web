

export class LocalBookModel {
    id: string;
    userId: string;
    code: string;
    day: Date;
    formattedDay: string;
    hours: string[];
    active: boolean;
    username?: string;
    address?: string;
    amount?: string;
    rangeTime?: string;
    createdAt: Date;
    formattedDate: string;
}
