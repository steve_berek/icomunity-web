export class CompanyModel {
  id?: string;
  name: string;
  address: string;
  postalCode: string;
  responsibleName: string;
  responsibleEmail: string;
  cif: string;
  monthlyBillingAmount: string;
  monthlyBillingDescription: string;
  logoUrl: string;
  country: string;
  city: string;
  phone: string;
  usersLimit: number;
}
