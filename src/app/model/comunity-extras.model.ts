import { PLANS_TYPE_ENUM } from '../common/enums/plans-type.enum';

export class ComunityExtrasModel {
  id: number;
  UnitPriceLocal: string;
  UnitPriceLocalAddPlus: string;
  UnitPriceLocalAddon: string;
  UnitPriceLocalMtp: string;
  UnitPricePadel: string;
  UnitPriceTenis: string;
  christmasActive: boolean;
  code_comunity: string;
  documentoActive: number;
  hoursLocal: string[];
  hoursPadel: string[];
  hoursPiscina: string[];
  hoursSolarium: string[];
  hoursTenis: string[];
  incidenciasActive: number;
  informacionActive: number;
  localActive: boolean;
  localPay: boolean;
  micasaActive: boolean;
  noticiasActive: boolean;
  padelActive: boolean;
  piscinaActive: boolean;
  piscinaHouseLimit: number;
  piscinaLimit: number;
  solariumActive: boolean;
  solariumHouseLimit: number;
  solariumLimit: number;
  telefonoActive: number;
  tenisActive: boolean;
  plan: PLANS_TYPE_ENUM;
}
