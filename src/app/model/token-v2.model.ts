export class TokenV2Model {
  accessToken: string;
  tokenType: string;
  refreshToken: string;
}
