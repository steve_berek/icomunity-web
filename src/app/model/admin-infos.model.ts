export class AdminInfosModel {
  id: string;
  name: string;
  admin_name: string;
  admin_email: string;
  admin_phone: string;
  admin_adress: string;

  constructor(initialValues?: Partial<AdminInfosModel> | null) {
    if (initialValues) {
      Object.assign(this, initialValues);
    }
  }
}
