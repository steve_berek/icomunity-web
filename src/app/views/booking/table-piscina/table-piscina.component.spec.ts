import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TablePiscinaComponent } from './table-piscina.component';

describe('TablePiscinaComponent', () => {
  let component: TablePiscinaComponent;
  let fixture: ComponentFixture<TablePiscinaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TablePiscinaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablePiscinaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
