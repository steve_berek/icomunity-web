import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TablePadelComponent } from './table-padel.component';

describe('TablePadelComponent', () => {
  let component: TablePadelComponent;
  let fixture: ComponentFixture<TablePadelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TablePadelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablePadelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
