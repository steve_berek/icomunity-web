import { DatePipe } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { MatDatepickerInputEvent } from '@angular/material';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { ComunidadModel } from '../../../model/comunidad.model';
import { UserModel } from '../../../model/vecino.model';
import { PadelModel } from '../../../models/padel.model';
import { UtilsService } from '../../../services/utils.service';
import { GetPadels } from '../../home/store/home.actions';
import { FILTRO_FECHA_ITEMS, SET_DATE_PNPUT } from '../booking.constants';

@Component({
  selector: 'app-table-padel',
  templateUrl: './table-padel.component.html',
  styleUrls: ['./table-padel.component.scss']
})
export class TablePadelComponent implements OnInit {
  @Select((state) => state.core.padels) padels$: Observable<any>;
  @Input() users: UserModel[] = [];
  @Input() selectedComunity: ComunidadModel;
  data: PadelModel[] = null;
  originalData: PadelModel[] = null;
  searchValue = '';
  selectedItem: any;
  filtroFechaItems = FILTRO_FECHA_ITEMS;
  setDateInput = SET_DATE_PNPUT;
  selectedFechaOption = '';

  constructor(private store: Store, private datePipe: DatePipe, private utilService: UtilsService) {}
  ngOnInit() {
    this.padels$.subscribe((items) => {
      if (items && items.length > 0) {
        this.data = this.mapPadelBooking(items);
        this.originalData = Object.assign(this.data);
      } else {
        this.data = [];
      }
    });
    this.refresh();
  }
  clearFilters() {
    this.resetSearch();
    this.refresh();
    this.selectedFechaOption = '';
  }
  exitDetail(done) {
    if (done) {
      this.refresh();
    }
    this.selectedItem = null;
  }
  selectItemPadel(item) {
    this.selectedItem = item;
    this.selectedItem.comunidad = this.selectedComunity;
  }
  refresh() {
    this.store.dispatch(new GetPadels());
  }
  resetSearch(): void {
    this.searchValue = '';
    this.search('');
  }

  search(value: string): void {
    this.searchValue = value;
    setTimeout(() => {
      this.data = this.originalData.filter((item: PadelModel) =>
        item.name
          .trim()
          .toLowerCase()
          .includes(this.searchValue.trim().toLowerCase())
      );
    }, 500);
  }

  mapPadelBooking(items) {
    const result: PadelModel[] = [];
    for (const book of items) {
      const hours = book.hour.split(',');
      const size = hours.length;
      const countHours = `${size}`;
      const strhours = `${hours[0]}h - ${Number(hours[size - 1]) + 1}h`;
      const item: PadelModel = {
        id: book._id,
        code: book.code,
        name: '',
        email: '',
        adress: '',
        day: book.day,
        hours: strhours,
        countHours: countHours,
        active: book.active,
        expired: book.expired,
        createdAt: book.createdAt
      };
      for (const user of this.users) {
        if (user.code_house === book.userId) {
          item.name = user.name;
          item.email = user.email;
          item.adress = user.adress;
        }
      }
      const exist = result.findIndex((el) => el.id === item.id);
      if (exist === -1) {
        result.push(item);
      }
    }
    return result;
  }

  onDateFilterChange(event) {
    this.data = this.utilService.onDateFilterChange(event, this.originalData);
    this.clearDatePickerInput();
  }

  addEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    const value = event.value;
    const result = [];
    if (type === 'in') {
      this.setDateInput.in.bool = true;
      this.setDateInput.in.value = value;
    } else if (type === 'fin') {
      this.setDateInput.fin.bool = true;
      this.setDateInput.fin.value = value;
    }
    if (this.setDateInput.in.bool && this.setDateInput.fin.bool) {
      this.originalData.forEach((item) => {
        const fechatime = new Date(item.day).getTime();
        const intime = this.setDateInput.in.value.getTime();
        const fintime = this.setDateInput.fin.value.getTime();
        if (intime < fechatime && fechatime < fintime) {
          result.push(item);
        }
      });
      this.data = result;
    }
  }
  clearDatePickerInput() {
    this.setDateInput = {
      in: {
        bool: false,
        value: new Date()
      },
      fin: {
        bool: false,
        value: new Date()
      }
    };
  }

  formatDate(date) {
    return this.datePipe.transform(date, 'yyyy-MM-dd HH:mm:ss');
  }
}
