import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableSolariumComponent } from './table-solarium.component';

describe('TableSolariumComponent', () => {
  let component: TableSolariumComponent;
  let fixture: ComponentFixture<TableSolariumComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableSolariumComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableSolariumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
