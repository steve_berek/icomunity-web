import { DatePipe } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { MatDatepickerInputEvent } from '@angular/material';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { ComunidadModel } from '../../../model/comunidad.model';
import { UserModel } from '../../../model/vecino.model';
import { SolariumModel } from '../../../models/solarium.model';
import { UtilsService } from '../../../services/utils.service';
import { GetSolariums } from '../../home/store/home.actions';
import { FILTRO_FECHA_ITEMS, SET_DATE_PNPUT } from '../booking.constants';

@Component({
  selector: 'app-table-solarium',
  templateUrl: './table-solarium.component.html',
  styleUrls: ['./table-solarium.component.scss']
})
export class TableSolariumComponent implements OnInit {
  @Select((state) => state.core.solariums) solariums$: Observable<any>;
  @Input() users: UserModel[] = [];
  @Input() selectedComunity: ComunidadModel;
  data: SolariumModel[] = null;
  originalData: SolariumModel[] = null;
  searchValue = '';
  selectedItem: any;
  filtroFechaItems = FILTRO_FECHA_ITEMS;
  setDateInput = SET_DATE_PNPUT;
  selectedFechaOption = '';

  constructor(private store: Store, private datePipe: DatePipe, private utilService: UtilsService) {}

  ngOnInit() {
    this.solariums$.subscribe((items) => {
      if (items && items.length > 0) {
        this.data = this.mapSOlariumBooking(items);
        this.originalData = Object.assign(this.data);
      } else {
        this.data = [];
      }
    });
    this.refresh();
  }
  clearFilters() {
    this.resetSearch();
    this.refresh();
    this.selectedFechaOption = '';
  }
  exitDetail(done) {
    if (done) {
      this.refresh();
    }
    this.selectedItem = null;
  }
  selectItemPadel(item) {
    this.selectedItem = item;
    this.selectedItem.comunidad = this.selectedComunity;
  }
  refresh() {
    this.store.dispatch(new GetSolariums());
  }
  resetSearch(): void {
    this.searchValue = '';
    this.search('');
  }

  search(value: string): void {
    this.searchValue = value;
    setTimeout(() => {
      this.data = this.originalData.filter((item: SolariumModel) =>
        item.username
          .trim()
          .toLowerCase()
          .includes(this.searchValue.trim().toLowerCase())
      );
    }, 500);
  }

  mapSOlariumBooking(items) {
    const result: SolariumModel[] = [];
    for (const book of items) {
      this.users.forEach((user) => {
        if (user.code_house === book.userId) {
          const item: SolariumModel = {
            id: book._id,
            code: book.code,
            username: book.username,
            useraddress: book.useraddress,
            persons: book.persons,
            day: book.day,
            userId: book.userId,
            hour: `${book.hour}h`,
            active: book.active,
            expired: book.expired,
            createdAt: book.createdAt
          };
          const exist = result.findIndex((el) => el.id === item.id);
          if (exist === -1) {
            result.push(item);
          }
        }
      });
    }
    return result;
  }

  onDateFilterChange(event) {
    this.data = this.utilService.onDateFilterChange(event, this.originalData);
    this.clearDatePickerInput();
  }

  addEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    const value = event.value;
    const result = [];
    if (type === 'in') {
      this.setDateInput.in.bool = true;
      this.setDateInput.in.value = value;
    } else if (type === 'fin') {
      this.setDateInput.fin.bool = true;
      this.setDateInput.fin.value = value;
    }
    if (this.setDateInput.in.bool && this.setDateInput.fin.bool) {
      this.originalData.forEach((item) => {
        const fechatime = new Date(item.day).getTime();
        const intime = this.setDateInput.in.value.getTime();
        const fintime = this.setDateInput.fin.value.getTime();
        if (intime < fechatime && fechatime < fintime) {
          result.push(item);
        }
      });
      this.data = result;
    }
  }
  clearDatePickerInput() {
    this.setDateInput = {
      in: {
        bool: false,
        value: new Date()
      },
      fin: {
        bool: false,
        value: new Date()
      }
    };
  }

  formatDate(date) {
    return this.datePipe.transform(date, 'yyyy-MM-dd HH:mm:ss');
  }
}
