import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingsMobileComponent } from './settings-mobile.component';

describe('SettingsMobileComponent', () => {
  let component: SettingsMobileComponent;
  let fixture: ComponentFixture<SettingsMobileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SettingsMobileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsMobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
