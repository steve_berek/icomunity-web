import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageComunitiesComponent } from './manage-comunities.component';

describe('ManageComunitiesComponent', () => {
  let component: ManageComunitiesComponent;
  let fixture: ComponentFixture<ManageComunitiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageComunitiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageComunitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
