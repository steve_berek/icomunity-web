export class GetInviteUsers {
  static readonly type = '[GESTION] GetInviteUsers';
  constructor(public code: string) {}
}

export class SendInvitesUsers {
  static readonly type = '[GESTION] SendInvitesUsers';
  constructor(public dto) {}
}
