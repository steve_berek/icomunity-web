import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router, CanActivate } from '@angular/router';
import { Observable } from 'rxjs';
import { Select } from '@ngxs/store';
import { AdminUserModel } from '../../model/admin-user.model';

@Injectable()
export class AdminAuthGuard implements CanActivate {
  @Select((state) => state.core.adminUser) adminUser$: Observable<any>;
  path: ActivatedRouteSnapshot[];
  route: ActivatedRouteSnapshot;
  adminUser: AdminUserModel;
  constructor(private router: Router) {
    this.adminUser$.subscribe((adminUser) => {
      if (adminUser) {
        this.adminUser = adminUser;
      }
    });
  }

  canActivate(): Observable<boolean> | boolean {
    if (this.adminUser.mainRole === 'THOR') {
      return true;
    } else {
      return false;
    }
  }
}
