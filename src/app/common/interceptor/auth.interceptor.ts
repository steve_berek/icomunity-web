import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SessionService } from '../../services/session/session.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private sessionService: SessionService) {}
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!request.url.includes('admin-user') && this.sessionService.fetchAutToken()) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${this.sessionService.fetchAutToken()}`
        }
      });
    }
    return next.handle(request);
  }
}
