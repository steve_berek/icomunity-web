export enum RESERVAS_TYPE_ENUM {
  LOCAL = 'LOCAL',
  PADEL = 'PADEL',
  TENIS = 'TENIS',
  PISCINA = 'PISCINA',
  SOLARIUM = 'SOLARIUM'
}
