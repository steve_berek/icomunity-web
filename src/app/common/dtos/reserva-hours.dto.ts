import { RESERVAS_TYPE_ENUM } from '../enums/reservas-type.enum';

export class ReservaHoursDto {
  type: RESERVAS_TYPE_ENUM;
  hours: string[];
}
