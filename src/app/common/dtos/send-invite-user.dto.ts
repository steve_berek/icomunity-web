export class SendInviteUserDto {
  code: string;
  comunity: string;
  emails: string[];
  key?: string;
}
