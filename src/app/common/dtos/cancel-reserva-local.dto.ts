export class CancelReservaLocalDto {
  id: string;
  code: string;
  comunidad: string;
  admin_email: string;
  username: string;
  adress: string;
  email: string;
  emails: string[];
  fecha: string;
  hora: string;
  paymentback: string;
}
