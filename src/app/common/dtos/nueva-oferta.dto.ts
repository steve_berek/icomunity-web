export class NuevaOfertaDto {
  type: string;
  clientname: string;
  title: string;
  description: string;
  imageUrl: string;
  url?: string;
  phone?: string;
  discount: string;
  qrcode: string;
  from: Date;
  to: Date;
}
