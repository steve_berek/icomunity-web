import { NgModule } from '@angular/core';
import { AssetToFullUrl } from './assetToFullUrl.pipe';
import { SafeHtmlPipe } from './safeHtml.pipe';
import { CustomDatePipe } from './dateTransform.pipe';
import { FileSizePipe } from './filesize.pipe';
@NgModule({
  declarations: [AssetToFullUrl, SafeHtmlPipe, CustomDatePipe, FileSizePipe],
  exports: [AssetToFullUrl, SafeHtmlPipe, CustomDatePipe, FileSizePipe]
})
export class PipesModule {}
