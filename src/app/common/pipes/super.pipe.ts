import { DatePipe } from '@angular/common';


export class SuperPipe {

    pipeFormatDate(date: Date): string {
        const pipe = new DatePipe('en-US');
        return pipe.transform(date, 'dd/MM/yyyy HH:mm');
    }

    pipeFormatDateNoTime(date: Date): string {
        const pipe = new DatePipe('en-US');
        return pipe.transform(date, 'dd/MM/yyyy');
    }
}
