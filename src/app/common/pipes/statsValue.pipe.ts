import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
  name: 'statsValue'
})
export class StatsValuePipe implements PipeTransform {
  transform(value: number, args?: any): any {
    const pValue = (value * 100) / 1000;
    return `--p:${pValue};--c:orange;--w:100px`;
  }
}
