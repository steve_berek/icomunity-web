export class MailingToken {
  accessToken: string;
  tokenType: string;
  refreshToken: string;
}
