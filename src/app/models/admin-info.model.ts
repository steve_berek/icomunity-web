export class AdminInfoModel {
    id: string;
    type: string;
    type_comunity: string;
    code: string;
    name: string;
    admin_name: string;
    admin_email: string;
    admin_phone: string;
    admin_adress: string;
    com_image: string;
    forfait: string;
}