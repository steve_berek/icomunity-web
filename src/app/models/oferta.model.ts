export class OfertaModel {
  id: string;
  clientID: string;
  type: string;
  clientname: string;
  title: string;
  description: string;
  imageUrl: string;
  url: string;
  phone: string;
  discount: string;
  qrcode: string;
  from: Date;
  to: Date;
  qrcodeUrl?: any;
  createdat: Date;
  updatedAt: Date;
}
