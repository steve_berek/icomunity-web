export class IncidenciModel{
    code_comunity:string;
    image:string;
    category:string;
    type:string;
    username:string;
    adress:string;
    description:string;
    created_at:string;
    create:string;
    state:number;
    oculto:number;
}