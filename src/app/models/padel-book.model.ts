export class LocalBookModel {
  id: string;
  userID: string;
  code: string;
  day: string;
  hours: string[];
  active: boolean;
  createdAt: Date;
}
