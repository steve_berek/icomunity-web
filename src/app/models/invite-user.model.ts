export class InviteUser {
  id: string;
  code: string;
  comunity: string;
  createdAt: string;
  updatedAt: string;
  email: string;
  sent: boolean;
}
