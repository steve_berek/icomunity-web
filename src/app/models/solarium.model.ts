export class SolariumModel {
  id: string;
  code: string;
  username: string;
  userId: string;
  useraddress: string;
  persons: number;
  day: string;
  hour: string;
  active: boolean;
  expired?: boolean;
  createdAt: Date;
}
