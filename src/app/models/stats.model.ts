export class StatsModel {
  IncidenciasCountTotal: number;
  IncidenciasCountMonth: number;
  IncidenciasPendientesCountMonth: number;
  IncidenciasCountYear: number;
  IncidenciasTotal: number;
  localTotal: number;
  localMonth: number;
  localMonthCancelled: number;
}
