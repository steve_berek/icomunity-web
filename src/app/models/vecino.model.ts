export class VecinoModel {
    name: string;
    adress: string;
    planta: string;
    door: string;
    email: string;
    phone: string;
    code_comunity: string;
    code_house: string;
    fb_token: string;
    state:number;
}