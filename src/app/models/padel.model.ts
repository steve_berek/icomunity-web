export class PadelModel {
  id: string;
  code: string;
  name: string;
  email: string;
  adress: string;
  day: string;
  hours: string;
  countHours?: string;
  active: boolean;
  expired: boolean;
  createdAt: Date;
}
